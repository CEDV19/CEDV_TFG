# Motor Arena

The final Game and project of the course of [expert in video game development](http://cedv.uclm.es/). Motor Arena is a multiplayer game that is an adaptation of the motorcycle fights in the [Disney film Tron](https://en.wikipedia.org/wiki/Tron) using [Unreal Engine 4](https://www.unrealengine.com).

There is available a [video of the project](https://www.dropbox.com/s/bk1bnk7fwlg6dw9/MotorArena.mp4?dl=0).

## Releases

You can download the lastest Motor Arena Standalone: [MotorArenaStandalone_Win64.zip](https://www.dropbox.com/s/cihgurzp2jp4uhv/MotorArenaStandalone_Win64.zip?dl=0)

## Build

Motor Arena is developed using Unreal Engine 4.22 version. Hence, If you want to open, contribute or use it in your projects, you only need to open the project using this version.

## Documentation

You can access the full Spanish documentation of Motor Arena: [MotorArenaDocumentation](https://www.dropbox.com/s/aymss80c8q9jgbx/MotorArenaDocumentacion.pdf?dl=0).

In Addition, the [repository of the documentation made in Latex](https://gitlab.com/CEDV19/CEDV_TFG_Documentation) is available for everyone.

## Pictures

![](https://i.imgur.com/PC4KB45.png)

## License

Battleship is provided under [GNU General Public License Version 3](https://gitlab.com/CEDV19/CEDV_TFG/blob/master/LICENSE).