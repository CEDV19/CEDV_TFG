/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#include "BorjaMotorcyclePawn.h"
#include "Camera/CameraComponent.h"
#include "Components/SceneComponent.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/MaterialInstance.h"
#include "ActorComponents/WallGeneratorActorComponent.h"
#include "GameStates/BattleGameState.h"
#include "MotorArenaGameInstance.h"
#include "ConstructorHelpers.h"

// ------------------------------- Struct FMovement -------------------------------

FDirection::FDirection() { CurrentDirection = 0; }

FDirection::FDirection(const TArray<FVector>& Directions, const uint8 StartDirectionInDirections)
{
	SetDirections(Directions, StartDirectionInDirections);
}

void FDirection::SetDirections(const TArray<FVector>& Directions, const uint8 StartDirectionInDirections)
{
	this->CurrentDirection = StartDirectionInDirections;
	this->Directions = Directions;
}

void FDirection::SetRotations(const TArray<FRotator>& Rotations, const FVector ForwardDirection,
	uint8 StartDirectionInDirections)
{
	this->Rotations = Rotations;
	this->CurrentDirection = StartDirectionInDirections;
	for (FRotator Rotation : Rotations) 
	{
		Directions.Add(Rotation.RotateVector(ForwardDirection));
	}
}

bool FDirection::Equals(FDirection& OtherDirection)
{
	return CurrentDirection == OtherDirection.CurrentDirection;
}

FVector FDirection::MoveDirection(FDirection& OtherDirection)
{
	int8 RightIndex = NormalizeCurrentDirectionIndex(CurrentDirection + 1);
	int8 LeftIndex = NormalizeCurrentDirectionIndex(CurrentDirection - 1);
	while(true)
	{
		if (RightIndex == OtherDirection.CurrentDirection) return Directions[RightIndex];
		if (LeftIndex == OtherDirection.CurrentDirection) return Directions[LeftIndex];
		RightIndex = NormalizeCurrentDirectionIndex(++RightIndex);
		LeftIndex = NormalizeCurrentDirectionIndex(--LeftIndex);
	}
}

FVector FDirection::GetDirection()
{
	return Directions[CurrentDirection];
}

FRotator FDirection::GetRotation()
{
	return Rotations[CurrentDirection];
}

FVector FDirection::MoveRightDirection()
{
	++CurrentDirection;
	return MoveDirection();
}

FVector FDirection::MoveLeftDirection()
{
	--CurrentDirection;
	return MoveDirection();
}

FVector FDirection::SetDirection()
{
	return MoveDirection();
}

int8 FDirection::NormalizeCurrentDirectionIndex(int8 Index)
{
	if (Index == Directions.Num()) Index = 0;
	else if (Index == -1) Index = Directions.Num() - 1;
	return Index;
}

FVector FDirection::MoveDirection()
{
	CurrentDirection = NormalizeCurrentDirectionIndex(CurrentDirection);
	return Directions[CurrentDirection];
}

// ------------------------------- Class BorjaMotorcyclePawn -------------------------------

// Sets default values
ABorjaMotorcyclePawn::ABorjaMotorcyclePawn(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Tick parameters
	PrimaryActorTick.bCanEverTick = true;

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;
	
	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure Scene Component
	SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot"));
	RootComponent = SceneRoot;

	// Configure Motorcycle Mesh
	MotorcycleMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Motorcycle"));
	MotorcycleMesh->SetupAttachment(SceneRoot);
	auto MotorcycleAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("UStaticMesh'/Game/Models/Motorcycle/Mesh/SM_Motorcycle'"));
	if (MotorcycleAsset.Succeeded())
	{
		MotorcycleMesh->SetStaticMesh(MotorcycleAsset.Object);
		MotorcycleMesh->SetRelativeLocation(FVector(16.f, 0.f, -32.f));
		MotorcycleMesh->SetRelativeScale3D(FVector(0.5, 0.5, 0.5));
		MotorcycleMesh->SetCollisionProfileName(TEXT("NoCollision"));
	}

	// Configure Borja Mesh
	BorjaMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Borja"));
	BorjaMesh->SetupAttachment(MotorcycleMesh);
	auto BorjaMeshAsset = ConstructorHelpers::FObjectFinder<USkeletalMesh>(TEXT("SkeletalMesh'/Game/Models/Borja/Mesh/SK_Borja'"));
	if(BorjaMeshAsset.Succeeded())
	{
		BorjaMesh->SetSkeletalMesh(BorjaMeshAsset.Object);
		BorjaMesh->SetRelativeLocationAndRotation(FVector(397.f, 0.f, 6.f), FRotator(0.f, 180.f, 0.f));
		BorjaMesh->SetSimulatePhysics(false);
		BorjaMesh->SetCollisionProfileName(TEXT("NoCollision"));
	}

	// Configure core collisions of the pawn
	CoreTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("CoreTrigger"));
	CoreTrigger->SetupAttachment(RootComponent);CoreTrigger->SetWorldLocation(FVector(150.f, 0.f, 50.f));
	CoreTrigger->SetBoxExtent(FVector(100.f, 40.f, 40.f));
	CoreTrigger->SetCollisionProfileName(TEXT("Trigger"));

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(SceneRoot);
	CameraBoom->SetRelativeLocation(FVector(180.f, 0.f, 100.f));
	CameraBoom->TargetArmLength = 400.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a WallGenerator component
	WallGenerator = CreateDefaultSubobject<UWallGeneratorActorComponent>(TEXT("WallGenerator"));

	SetReplicates(true);
	SetReplicateMovement(true);
	SceneRoot->SetIsReplicated(true);
	MotorcycleMesh->SetIsReplicated(true);

	// Set Class variables
	bIsDead = false;
	bIsGamePlaying = false;
}

void ABorjaMotorcyclePawn::Initialize(FVector Location, FVector FordwardVector, class UMaterialInstance* PlayerColor, class ABattleGameState* GameState)
{
	// Create all structures to the movement of the Character
	TArray<FRotator> Rotations{
		FRotator(0, 0, 0),
		FRotator(0, 90, 0),
		FRotator(0, 180, 0),
		FRotator(0, 270, 0)
	};
	CurrentDirection.SetRotations(Rotations, GetActorForwardVector());

	// Set the closes direction
	int8 ClosesRotationIndex = 0;
	float AngleValueFromTwoVectors = -10000;
	for(int8 index = 0; index < Rotations.Num(); ++index)
	{
		float AngleValue = FVector::DotProduct(CurrentDirection.Directions[index], FordwardVector);
		if(AngleValue > AngleValueFromTwoVectors)
		{
			AngleValueFromTwoVectors = AngleValue;
			ClosesRotationIndex = index;
		}
	}
	CurrentDirection.CurrentDirection = ClosesRotationIndex;
	SetActorLocationAndRotation(Location, CurrentDirection.Rotations[CurrentDirection.CurrentDirection]);

	// Set Game State
	BattleGameState = GameState;

	// Set colors
	CurrentColor = PlayerColor;
	SetBorjaMaterials();
	
	// Create first Wall
	WallGenerator->CreateNewWallPart(GetActorLocation(), GetActorRotation(), CurrentColor.Get());
}

void ABorjaMotorcyclePawn::MoveRight()
{
	if(!bIsDead && bIsGamePlaying) LateralMoveInServer(true);
}

void ABorjaMotorcyclePawn::MoveLeft()
{
	if(!bIsDead && bIsGamePlaying) LateralMoveInServer(false);
}

void ABorjaMotorcyclePawn::StartGame()
{
	bIsGamePlaying = true;
}

bool ABorjaMotorcyclePawn::IsDead()
{
	return bIsDead;
}

void ABorjaMotorcyclePawn::FinishGame()
{
	bIsGamePlaying = false;
}

FVector ABorjaMotorcyclePawn::MoveDirection(ABorjaMotorcyclePawn* OtherPawn)
{
	return CurrentDirection.MoveDirection(OtherPawn->CurrentDirection);
}


void ABorjaMotorcyclePawn::EliminateInGame()
{
	bIsDead = true;
	SetActorLocation(FVector(-999.f, -999.f, -999.f));
	WallGenerator->RemoveAllWallGenerated();
}

void ABorjaMotorcyclePawn::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if(!bIsDead && bIsGamePlaying) SetActorLocation(GetActorLocation() + CurrentDirection.GetDirection() * MotorArenaGameInstance->BattleSpeed * DeltaSeconds);
}

void ABorjaMotorcyclePawn::BeginPlay()
{
	Super::BeginPlay();

	MotorArenaGameInstance = GetGameInstance<UMotorArenaGameInstance>();

	// Set overlaps delegates
	CoreTrigger->OnComponentBeginOverlap.AddDynamic(this, &ABorjaMotorcyclePawn::OnOverlapBegin);
}

void ABorjaMotorcyclePawn::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(!bIsDead && 
		BattleGameState.IsValid() && 
		!OtherActor->ActorHasTag("IgnoreCollision")
	)
	{
		BattleGameState->PlayerIsEliminated(this);
	}
}

void ABorjaMotorcyclePawn::LateralMoveInServer_Implementation(bool Right)
{
	if (Right) CurrentDirection.MoveRightDirection();
	else CurrentDirection.MoveLeftDirection();
	SetActorRotation(CurrentDirection.GetRotation());
	WallGenerator->CreateNewWallPart(GetActorLocation(), GetActorRotation(), CurrentColor.Get());
}

bool ABorjaMotorcyclePawn::LateralMoveInServer_Validate(bool Right)
{
	return true;
}

void ABorjaMotorcyclePawn::SetBorjaMaterials()
{
	MotorcycleMesh->SetMaterial(0, CurrentColor.Get());
	BorjaMesh->SetMaterial(1, CurrentColor.Get());
}

void ABorjaMotorcyclePawn::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ABorjaMotorcyclePawn::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ABorjaMotorcyclePawn::LookUpAtRate);

	// Lateral Moves
	PlayerInputComponent->BindAction("MoveRight", IE_Pressed, this, &ABorjaMotorcyclePawn::MoveRight);
	PlayerInputComponent->BindAction("MoveLeft", IE_Pressed, this, &ABorjaMotorcyclePawn::MoveLeft);
}

void ABorjaMotorcyclePawn::TurnAtRate(float Rate)
{
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ABorjaMotorcyclePawn::LookUpAtRate(float Rate)
{
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}


void ABorjaMotorcyclePawn::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABorjaMotorcyclePawn, CurrentColor);
	DOREPLIFETIME(ABorjaMotorcyclePawn, CurrentDirection);
	DOREPLIFETIME(ABorjaMotorcyclePawn, bIsGamePlaying);
}
