/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TimerManager.h"
#include "AIActor.generated.h"

/**
 * The abstract AI with the basic and common logic to inherit to create better AI
 */
UCLASS()
class MOTORARENA_API AAIActor : public AActor
{
	GENERATED_BODY()
	
public:	

	AAIActor();

	UFUNCTION(BlueprintCallable, Category = "AIActor")
		virtual void Initialize(
			const class ABattleGameState* GameState,
			const class ABorjaMotorcyclePawn* Pawn = nullptr
		);

	UFUNCTION(BlueprintCallable, Category = "AIActor")
		virtual void SetPossesedPawnAndExecuteBehavior(const class ABorjaMotorcyclePawn* Pawn);

	UFUNCTION(BlueprintCallable, Category = "AIActor")
		virtual void ExecuteBehavior();

	UFUNCTION(BlueprintCallable, Category = "AIActor")
		void StopTimers();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AIActor")
		TWeakObjectPtr<class ABorjaMotorcyclePawn> PossesedPawn;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AIActor")
		TWeakObjectPtr<class ABattleGameState> BattleGameState;

	// Behaviour and collision public variables to create the behavior in the inherit classes
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AIActor")
		float TimeToWaitToExecuteBehavior;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AIActor")
		float TimeToWaitToExecuteBehaviorFirstTime;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AIActor")
		bool bExecuteBehaviorInLoop;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AIActor")
		float TimeToWaitToExecuteCollisionDetections;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AIActor")
		float TimeToWaitToExecuteCollisionsDetectionFirstTime;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AIActor")
		bool bExecuteColisionsDetectionInLoop;

protected:	

	virtual void BeginPlay() override;

	// Behavior and collision methods
	virtual void Behavior();
	virtual void CollisionDetection();
	virtual void FordwardCollisionDetection();

	// Movements methods
	void LateralsPrecautionsMovement(FVector NewForwardDirection);
	void WideMovement();

	// Behaviour and collision timers and variables to create the behavior in the inherit classes
	bool bCanExexuteBehavior;
	FTimerHandle MovementTimer;
	FTimerHandle CollisionsTimer;
	FTimerDelegate MovementTimerCallback;
	FTimerDelegate CollisionsTimerCallback;
};
