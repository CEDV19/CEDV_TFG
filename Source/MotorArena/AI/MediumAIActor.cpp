/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#include "MediumAIActor.h"
#include "GameStates/BattleGameState.h"
#include "Actors/Pawns/BorjaMotorcyclePawn.h"
#include "Functionalities.h"

AMediumAIActor::AMediumAIActor() : Super() {}

void AMediumAIActor::ExecuteBehavior()
{
	Super::ExecuteBehavior();

	// Set class variables
	bCorralPawn = false;
}

void AMediumAIActor::CorralPawn()
{
	if(PossesedPawn.IsValid() && ClosesPawn.IsValid() && !bCorralPawn)
	{
		if (!PossesedPawn->CurrentDirection.Equals(ClosesPawn->CurrentDirection))
		{
			const FVector NewDirection = PossesedPawn->MoveDirection(ClosesPawn.Get());
			LateralsPrecautionsMovement(NewDirection);
		}
		else bCorralPawn = true;
	}
}

void AMediumAIActor::ImitatePawnMovements()
{
	if(PossesedPawn.IsValid() && ClosesPawn.IsValid() && !PossesedPawn->CurrentDirection.Equals(ClosesPawn->CurrentDirection))
	{
		const FVector NewDirection = PossesedPawn->MoveDirection(ClosesPawn.Get());
		LateralsPrecautionsMovement(NewDirection);
	}
}

void AMediumAIActor::Behavior()
{
	Super::Behavior();
	
	if (PossesedPawn.IsValid())
	{
		float MinDistance = 0;
		ClosesPawn = Cast<ABorjaMotorcyclePawn>(UFunctionalities::MinActorDistance(PossesedPawn.Get(), BattleGameState->ActorsInGame, MinDistance));
		CorralPawn();
	}
}

void AMediumAIActor::FordwardCollisionDetection()
{
	bCorralPawn = false;
}
