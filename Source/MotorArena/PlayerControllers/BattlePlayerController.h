/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#pragma once

#include "GameFramework/PlayerController.h"
#include "BattlePlayerController.generated.h"

/**
 * The Player Controller used to play the main game of Motor Arena
 */
UCLASS()
class MOTORARENA_API ABattlePlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	ABattlePlayerController(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable, Category = "BattlePlayerController")
		void SetVieportToGame();

	UFUNCTION(BlueprintCallable, Category = LobbyPlayerController)
		void CreateMenus();

	UFUNCTION(NetMulticast, Reliable, WithValidation)
		void SetFinishMenu();

protected:

	void BeginPlay() override;

	// Auxiliary methods to create menus
	TSubclassOf<UUserWidget> GetMenuAssets(FString MenuAssetPath) const;
	UUserWidget* CreateMenu(TSubclassOf<class UUserWidget> WidgetMenu);

	// Menu variables
	TSubclassOf<class UUserWidget> UserUIClass, FinishUIClass;
	TWeakObjectPtr<class UUserWidget> UserUI, FinishUI;
};
