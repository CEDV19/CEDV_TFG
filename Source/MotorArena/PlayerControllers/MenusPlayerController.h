/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#pragma once

#include "GameFramework/PlayerController.h"
#include "MenusPlayerController.generated.h"

/**
 * This Player Controller is used to allow the user interact with the game menus and all its functionalities 
 */
UCLASS()
class MOTORARENA_API AMenusPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	AMenusPlayerController();

	UFUNCTION(BlueprintCallable, Category = MenusPlayerController)
		void CreateMenus();

	UFUNCTION(BlueprintCallable, Category = LobbyPlayerController)
		void SetViewport();

	// Methods to set menus to viewport
	UFUNCTION(BlueprintCallable, Category = MenusPlayerController)
		void SetMainMenuToViewport();

	UFUNCTION(BlueprintCallable, Category = MenusPlayerController)
		void SetPlayMenuToViewport();

	UFUNCTION(BlueprintCallable, Category = MenusPlayerController)
		void SetOptionsMenuToViewport();

	UFUNCTION(BlueprintCallable, Category = MenusPlayerController)
		void SetCreateGameMenuToViewport();

	UFUNCTION(BlueprintCallable, Category = MenusPlayerController)
		void SetJoinGameMenuToViewport();

	UFUNCTION(BlueprintCallable, Category = MenusPlayerController)
		void SetQuickPlayMenuToViewport();

	UFUNCTION(BlueprintCallable, Category = MenusPlayerController)
		void SetMenuToViewport(class UUserWidget* MenuWidget);
	
protected:
	void BeginPlay() override;

	// Auxiliary methods to create menus
	TSubclassOf<UUserWidget> GetMenuAssets(FString MenuAssetPath) const;
	UUserWidget* CreateMenu(TSubclassOf<class UUserWidget> WidgetMenu);

	// Menu variables
	TSubclassOf<class UUserWidget> MainMenuClass, PlayMenuClass, OptionsMenuClass, CreateGameMenuClass, JoinGameMenuClass, QuickPlayMenuClass;
	TWeakObjectPtr<class UUserWidget> CurrentMenu, MainMenu, PlayMenu, OptionsMenu, CreateGameMenu, JoinGameMenu, QuickPlayMenu;
};
