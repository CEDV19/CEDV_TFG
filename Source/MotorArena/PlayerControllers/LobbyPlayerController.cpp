/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#include "LobbyPlayerController.h"
#include "Blueprint/UserWidget.h"
#include "ConstructorHelpers.h"

ALobbyPlayerController::ALobbyPlayerController() : Super()
{
	// Get Classes of the menus to create
	const FString FolderPathOfWidgets = "/Game/UI/Menus/";
	HostMenuClass = GetMenuAssets(FolderPathOfWidgets + "W_LobbyHostMenu");
	ClientMenuClass = GetMenuAssets(FolderPathOfWidgets + "W_LobbyClientMenu");
}

void ALobbyPlayerController::SetViewport(const bool Client)
{
	CreateMenus();

	// Set properties of controller to use it interacting only with the menus
	FInputModeUIOnly Mode;
	Mode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
	SetInputMode(Mode);
	bShowMouseCursor = true;

	if (Client) SetClientMenuToViewport();
	else SetHostMenuToViewport();
}

void ALobbyPlayerController::CreateMenus()
{
	HostMenu = CreateMenu(HostMenuClass);
	ClientMenu = CreateMenu(ClientMenuClass);
}

void ALobbyPlayerController::SetHostMenuToViewport()
{
	if(!HostMenu->IsInViewport()) HostMenu->AddToViewport();
}

void ALobbyPlayerController::SetClientMenuToViewport()
{
	ClientMenu->AddToViewport();
}

UUserWidget* ALobbyPlayerController::GetHostMenu()
{
	return HostMenu.Get();
}

UUserWidget* ALobbyPlayerController::GetClientMenu()
{
	return ClientMenu.Get();
}

void ALobbyPlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (IsLocalPlayerController() &&
		(GetWorld()->GetNetMode() == NM_ListenServer ||
			GetWorld()->GetNetMode() == NM_DedicatedServer ||
			GetWorld()->GetNetMode() == NM_Standalone
			))
	{
		SetViewport(false);
	}
	else if (GetWorld()->GetNetMode() == NM_Client)
		SetViewport();
}

TSubclassOf<UUserWidget> ALobbyPlayerController::GetMenuAssets(const FString MenuAssetPath) const
{
	auto MenuAssetClass = ConstructorHelpers::FClassFinder<UUserWidget>(*MenuAssetPath);
	if (MenuAssetClass.Succeeded()) return MenuAssetClass.Class;
	UE_LOG(LogTemp, Warning, TEXT("Widget menu %s not found!"), *MenuAssetPath);
	return nullptr;
}

UUserWidget* ALobbyPlayerController::CreateMenu(TSubclassOf<UUserWidget> WidgetMenu)
{
	if (IsValid(WidgetMenu)) return CreateWidget<UUserWidget>(this, WidgetMenu);
	UE_LOG(LogTemp, Fatal, TEXT("Widget menu class %s not exist"), *WidgetMenu->GetName());
	return nullptr;
}
