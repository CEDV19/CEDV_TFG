/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). *
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#include "MenusPlayerController.h"
#include "Blueprint/UserWidget.h"
#include "ConstructorHelpers.h"

AMenusPlayerController::AMenusPlayerController() : Super()
{
	// Get Classes of the menus to create
	const FString FolderPathOfWidgets = "/Game/UI/Menus/";
	MainMenuClass = GetMenuAssets(FolderPathOfWidgets + "W_MainMenu");
	PlayMenuClass = GetMenuAssets(FolderPathOfWidgets + "W_PlayMenu");
	OptionsMenuClass = GetMenuAssets(FolderPathOfWidgets + "W_OptionsMenu");
	CreateGameMenuClass = GetMenuAssets(FolderPathOfWidgets + "W_CreateGameMenu");
	JoinGameMenuClass = GetMenuAssets(FolderPathOfWidgets + "W_JoinGameMenu");
	QuickPlayMenuClass = GetMenuAssets(FolderPathOfWidgets + "W_QuickPlayMenu");
}

void AMenusPlayerController::CreateMenus()
{
	MainMenu = CreateMenu(MainMenuClass);
	PlayMenu = CreateMenu(PlayMenuClass);
	OptionsMenu = CreateMenu(OptionsMenuClass);
	CreateGameMenu = CreateMenu(CreateGameMenuClass);
	JoinGameMenu = CreateMenu(JoinGameMenuClass);
	QuickPlayMenu = CreateMenu(QuickPlayMenuClass);
}

void AMenusPlayerController::SetViewport()
{
	CreateMenus();

	// Set properties of controller to use it interacting only with the menus
	FInputModeUIOnly Mode;
	Mode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
	SetInputMode(Mode);
	bShowMouseCursor = true;
	MainMenu->AddToViewport();
	CurrentMenu = MainMenu;
}

void AMenusPlayerController::SetMainMenuToViewport()
{
	if(!MainMenu.IsValid()) MainMenu = CreateMenu(MainMenuClass);
	SetMenuToViewport(MainMenu.Get());
}

void AMenusPlayerController::SetPlayMenuToViewport()
{
	if(!PlayMenu.IsValid()) PlayMenu = CreateMenu(PlayMenuClass);
	SetMenuToViewport(PlayMenu.Get());
}

void AMenusPlayerController::SetOptionsMenuToViewport()
{
	if(!OptionsMenu.IsValid()) OptionsMenu = CreateMenu(OptionsMenuClass);
	SetMenuToViewport(OptionsMenu.Get());
}

void AMenusPlayerController::SetCreateGameMenuToViewport()
{
	if(!CreateGameMenu.IsValid()) CreateGameMenu = CreateMenu(CreateGameMenuClass);
	SetMenuToViewport(CreateGameMenu.Get());
}

void AMenusPlayerController::SetJoinGameMenuToViewport()
{
	if(!JoinGameMenu.IsValid()) JoinGameMenu = CreateMenu(JoinGameMenuClass);
	SetMenuToViewport(JoinGameMenu.Get());
}

void AMenusPlayerController::SetQuickPlayMenuToViewport()
{
	if(!QuickPlayMenu.IsValid()) QuickPlayMenu = CreateMenu(QuickPlayMenuClass);
	SetMenuToViewport(QuickPlayMenu.Get());
}

void AMenusPlayerController::SetMenuToViewport(UUserWidget* MenuWidget)
{
	CurrentMenu->RemoveFromViewport();
	MenuWidget->AddToViewport();
	CurrentMenu = MenuWidget;
}

void AMenusPlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (IsLocalPlayerController() && 
		(GetWorld()->GetNetMode() == NM_ListenServer || 
			GetWorld()->GetNetMode() == NM_DedicatedServer || 
			GetWorld()->GetNetMode() == NM_Standalone
	))
	{
		SetViewport();
	}
	else if (GetWorld()->GetNetMode() == NM_Client)
		SetViewport();
}

TSubclassOf<UUserWidget> AMenusPlayerController::GetMenuAssets(const FString MenuAssetPath) const
{
	auto MenuAssetClass = ConstructorHelpers::FClassFinder<UUserWidget>(*MenuAssetPath);
	if (MenuAssetClass.Succeeded()) return MenuAssetClass.Class;
	UE_LOG(LogTemp, Warning, TEXT("Widget menu %s not found!"), *MenuAssetPath);
	return nullptr;
}

UUserWidget* AMenusPlayerController::CreateMenu(const TSubclassOf<UUserWidget> WidgetMenu)
{
	if (IsValid(WidgetMenu)) return CreateWidget<UUserWidget>(this, WidgetMenu);
	UE_LOG(LogTemp, Fatal, TEXT("Widget menu class %s not exist"), *WidgetMenu->GetName());
	return nullptr;
}
