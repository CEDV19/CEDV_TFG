/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#pragma once

#include "GameFramework/PlayerController.h"
#include "LobbyPlayerController.generated.h"

/**
 * This Player Controller is used to allow the user interact with the lobby 
 */
UCLASS()
class MOTORARENA_API ALobbyPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ALobbyPlayerController();

	UFUNCTION(BlueprintCallable, Category = LobbyPlayerController)
		void SetViewport(bool Client = true);

	UFUNCTION(BlueprintCallable, Category = LobbyPlayerController)
		void CreateMenus();

	// Methods to set menus to viewport
	UFUNCTION(BlueprintCallable, Category = LobbyPlayerController)
		void SetHostMenuToViewport();

	UFUNCTION(BlueprintCallable, Category = LobbyPlayerController)
		void SetClientMenuToViewport();

	// Methods to set menus to get widgets
	UFUNCTION(BlueprintCallable, Category = LobbyPlayerController)
		UUserWidget* GetHostMenu();

	UFUNCTION(BlueprintCallable, Category = LobbyPlayerController)
		UUserWidget* GetClientMenu();

protected:

	void BeginPlay() override;

	// Auxiliary methods to create menus
	TSubclassOf<UUserWidget> GetMenuAssets(FString MenuAssetPath) const;
	UUserWidget* CreateMenu(TSubclassOf<class UUserWidget> WidgetMenu);

	// Menu variables
	TSubclassOf<class UUserWidget> HostMenuClass, ClientMenuClass;
	TWeakObjectPtr<class UUserWidget> HostMenu, ClientMenu;
};
