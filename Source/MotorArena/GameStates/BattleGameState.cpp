/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#include "BattleGameState.h"
#include "Actors/Pawns/BorjaMotorcyclePawn.h"
#include "MotorArenaGameInstance.h"
#include "Functionalities.h"
#include "AIControllers/BattleAIController.h"
#include "PlayerControllers/BattlePlayerController.h"
#include "Materials/MaterialInstance.h"
#include "Engine/TargetPoint.h"
#include "GameFramework/Actor.h"
#include "EngineUtils.h"
#include "ConstructorHelpers.h"

ABattleGameState::ABattleGameState() : Super()
{
	// Get necessaries Blueprint Classes
	auto PlayerTarget = ConstructorHelpers::FClassFinder<ATargetPoint>(TEXT("TargetPoint'/Game/ActorsBlueprints/TargetPoints/BP_TargetPlayer'"));
	if (PlayerTarget.Succeeded()) {
		PlayerTargetClass = PlayerTarget.Class;
	}

	auto EnemyTarget = ConstructorHelpers::FClassFinder<ATargetPoint>(TEXT("TargetPoint'/Game/ActorsBlueprints/TargetPoints/BP_TargetAI'"));
	if (EnemyTarget.Succeeded()) {
		AITargetClass = EnemyTarget.Class;
	}

	// Initialize class variables
	NumberOfPlayersInGame = 0;
	StartCount = 5;
	bGameFinished = false;
}

void ABattleGameState::AddPlayerInGame(class ABattlePlayerController* BattlePlayerController, class UMaterialInstance* PlayerColor)
{
	if (!bGameFinished)
	{
		if (PlayerTargets.Num() == 0)
		{
			// Get all target player Points in the Map
			for (TActorIterator<ATargetPoint> TargetPointIterator(GetWorld()); TargetPointIterator; ++TargetPointIterator)
			{
				if (TargetPointIterator->IsA(PlayerTargetClass))
					PlayerTargets.Add(*TargetPointIterator);
			}
			UFunctionalities::ShuffleArray(PlayerTargets);
		}

		TWeakObjectPtr<ABorjaMotorcyclePawn> PlayerPawn = GetWorld()->SpawnActor<ABorjaMotorcyclePawn>();
		PlayerPawn->Initialize(PlayerTargets[0]->GetActorLocation(), PlayerTargets[0]->GetActorForwardVector(), PlayerColor, this);
		PlayerTargets.RemoveAt(0);
		Players.Add(PlayerPawn.Get(), BattlePlayerController);
		ActorsInGame.Add(PlayerPawn.Get());
		BattlePlayerController->GetPawn()->Tags.Add(FName("IgnoreCollision"));
		BattlePlayerController->GetPawn()->SetActorLocation(FVector(0.f, 0.f, 750.f));
		PlayersSpectatorsPawns.Add(BattlePlayerController, BattlePlayerController->GetPawn());
		BattlePlayerController->GetPawn()->SetActorHiddenInGame(true);
		++NumberOfPlayersInGame;
		BattlePlayerController->Possess(PlayerPawn.Get());
	}
}

void ABattleGameState::PlayerIsEliminated_Implementation(ABorjaMotorcyclePawn* Pawn)
{
	if (!bGameFinished)
	{
		if (Players.Contains(Pawn))
			Players[Pawn]->Possess(PlayersSpectatorsPawns[Players[Pawn]]);
		else
			AIs.Remove(Pawn);
		Pawn->EliminateInGame();
		--NumberOfPlayersInGame;
		if (ActorsInGame.Contains(Pawn)) ActorsInGame.Remove(Pawn);
		Pawn->Destroy();
		CheckStateOfGame();
	}
}

bool ABattleGameState::PlayerIsEliminated_Validate(ABorjaMotorcyclePawn* Pawn)
{
	return true;
}

void ABattleGameState::RemovePlayerInGame(ABattlePlayerController* BattlePlayerController)
{
	if (!bGameFinished)
	{
		if (Players.Contains(*Players.FindKey(BattlePlayerController))) Players.Remove(*Players.FindKey(BattlePlayerController));
		if (ActorsInGame.Contains(BattlePlayerController)) ActorsInGame.Remove(BattlePlayerController);
		if (PlayersSpectatorsPawns.Contains(BattlePlayerController)) PlayersSpectatorsPawns.Remove(BattlePlayerController);
		--NumberOfPlayersInGame;
		CheckStateOfGame();
	}
}

void ABattleGameState::AddAIInGame(ABattleAIController* BattleAIController, UMaterialInstance* PlayerColor)
{
	if (!bGameFinished)
	{
		if (AITargets.Num() == 0)
		{
			// Get all target AI Points in the Map
			for (TActorIterator<ATargetPoint> TargetPointIterator(GetWorld()); TargetPointIterator; ++TargetPointIterator)
			{
				if (TargetPointIterator->IsA(AITargetClass))
					AITargets.Add(*TargetPointIterator);
			}
			UFunctionalities::ShuffleArray(AITargets);
		}

		TWeakObjectPtr<ABorjaMotorcyclePawn> AIPawn = GetWorld()->SpawnActor<ABorjaMotorcyclePawn>();
		AIPawn->Initialize(AITargets[0]->GetActorLocation(), AITargets[0]->GetActorForwardVector(), PlayerColor, this);
		AITargets.RemoveAt(0);
		AIs.Add(AIPawn.Get(), BattleAIController);
		ActorsInGame.Add(AIPawn.Get());
		++NumberOfPlayersInGame;
		BattleAIController->OnPossess(AIPawn.Get());
	}
}

void ABattleGameState::StartGame()
{
	GetWorld()->GetTimerManager().SetTimer(
		SetGameTimer, 
		this,
		&ABattleGameState::SetGameStart,
		1.f, 
		true, 
		1.f
	);
}

void ABattleGameState::SetGameStart()
{
	if (StartCount <= -1)
	{
		TArray<ABorjaMotorcyclePawn*> PlayerPawns;
		Players.GetKeys(PlayerPawns);
		for (auto Player : PlayerPawns)
		{
			Player->StartGame();
		}

		TArray<ABorjaMotorcyclePawn*> AIPawns;
		AIs.GetKeys(AIPawns);
		for (auto AI : AIPawns)
		{
			AI->StartGame();
		}
		GetWorld()->GetTimerManager().ClearTimer(SetGameTimer);
	}
	else --StartCount;
}

void ABattleGameState::CheckStateOfGame_Implementation()
{
	if (!bGameFinished)
	{
		if (NumberOfPlayersInGame <= 1)
		{
			bGameFinished = true;
			for(auto AI : AIs)
			{
				AI.Value->StopAI();
			}
			TArray<ABattlePlayerController*> AllPlayers;
			PlayersSpectatorsPawns.GetKeys(AllPlayers);
			for (auto Player : AllPlayers)
			{
				Player->SetFinishMenu();
				TWeakObjectPtr<ABorjaMotorcyclePawn> Pawn = Cast<ABorjaMotorcyclePawn>(Player->GetPawn());
				if (Pawn.IsValid()) Pawn->FinishGame();
			}
		}
	}
}

bool ABattleGameState::CheckStateOfGame_Validate()
{
	return true;
}

void ABattleGameState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABattleGameState, NumberOfPlayersInGame);
	DOREPLIFETIME(ABattleGameState, StartCount);
	DOREPLIFETIME(ABattleGameState, bGameFinished);
}
