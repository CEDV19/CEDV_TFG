/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#pragma once

#include "GameFramework/GameStateBase.h"
#include "TimerManager.h"
#include "BattleGameState.generated.h"

/**
 * The Game State of the main game which responsibility is the correct workflow of the game 
 */
UCLASS()
class MOTORARENA_API ABattleGameState : public AGameStateBase
{
	GENERATED_BODY()

public:

	ABattleGameState();

	// Public methods
	UFUNCTION(BlueprintCallable, Category = "BattleGameState")
		void AddPlayerInGame(class ABattlePlayerController* BattlePlayerController, class UMaterialInstance* PlayerColor);

	UFUNCTION(Server, Reliable, WithValidation)
		void PlayerIsEliminated(class ABorjaMotorcyclePawn* Pawn);

	UFUNCTION(BlueprintCallable, Category = "BattleGameState")
		void RemovePlayerInGame(class ABattlePlayerController* BattlePlayerController);

	UFUNCTION(BlueprintCallable, Category = "BattleGameState")
		void AddAIInGame(class ABattleAIController* BattleAIController, class UMaterialInstance* PlayerColor);

	UFUNCTION(BlueprintCallable, Category = "BattleGameState")
		void StartGame();

	UFUNCTION(Server, Reliable, WithValidation)
		void CheckStateOfGame();

	// Variables
	UPROPERTY(VisibleAnywhere, Replicated, BlueprintReadOnly, Category = BattleGameState)
		uint8 NumberOfPlayersInGame;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = BattleGameState)
		TMap<class ABorjaMotorcyclePawn*, class ABattlePlayerController*> Players;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = BattleGameState)
		TMap<class ABorjaMotorcyclePawn*, class ABattleAIController*> AIs;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = BattleGameState)
		TMap<class ABattlePlayerController*, class APawn*> PlayersSpectatorsPawns;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = BattleGameState)
		TArray<class AActor*> ActorsInGame;

	UPROPERTY(VisibleAnywhere, Replicated, BlueprintReadOnly, Category = BattleGameState)
		int StartCount;

protected:

	void SetGameStart();

	UPROPERTY(Replicated)
		bool bGameFinished;

	// Target class from the level
	UPROPERTY()
		TSubclassOf<class ATargetPoint> PlayerTargetClass;

	UPROPERTY()
		TSubclassOf<class ATargetPoint> AITargetClass;

	// Targets from the level
	TArray<ATargetPoint*> PlayerTargets;
	TArray<ATargetPoint*> AITargets;

	// Start timer
	FTimerHandle SetGameTimer;
};
