/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "LobbyGameState.generated.h"

/**
 * The Game State of the game lobby which responsability is manage the connected clients and some configuration parameters 
 */
UCLASS()
class MOTORARENA_API ALobbyGameState : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	ALobbyGameState();

	UFUNCTION(BlueprintCallable, Category = LobbyGameState)
		void AddNewPlayer(class ALobbyPlayerController* NewPlayer);

	UFUNCTION(BlueprintCallable, Category = LobbyGameState)
		void RemovePlayer(class ALobbyPlayerController* ExitedPlayer);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = LobbyGameState)
		int NumberPlayers;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = LobbyGameState)
		int NumberAI;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = LobbyGameState)
		TArray<class ALobbyPlayerController*> PlayersConected;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = LobbyGameState)
		TWeakObjectPtr<class ALobbyPlayerController> ServerLobbyPlayerController;
};
