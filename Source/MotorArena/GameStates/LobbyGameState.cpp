/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#include "LobbyGameState.h"
#include "PlayerControllers/LobbyPlayerController.h"

ALobbyGameState::ALobbyGameState() : Super()
{
	// Class Variables
	NumberPlayers = 0;
	NumberAI = 0;
}

void ALobbyGameState::AddNewPlayer(ALobbyPlayerController* NewPlayer)
{
	if (NewPlayer != nullptr)
	{
		PlayersConected.Add(NewPlayer);
		++NumberPlayers;
	}
}

void ALobbyGameState::RemovePlayer(ALobbyPlayerController* ExitedPlayer)
{
	if (ExitedPlayer != nullptr)
	{
		PlayersConected.Remove(ExitedPlayer);
		--NumberPlayers;
	}
}
