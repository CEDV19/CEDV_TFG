/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#include "LobbyGameMode.h"
#include "PlayerControllers/LobbyPlayerController.h"
#include "GameStates/LobbyGameState.h"

ALobbyGameMode::ALobbyGameMode() : Super()
{
	PlayerControllerClass = ALobbyPlayerController::StaticClass();
	GameStateClass = ALobbyGameState::StaticClass();
}

void ALobbyGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	
	if (!LobbyGameState.IsValid())
	{
		LobbyGameState = Cast<ALobbyGameState>(GameState);
		LobbyGameState->ServerLobbyPlayerController = Cast<ALobbyPlayerController>(NewPlayer);
	}
	LobbyGameState->AddNewPlayer(Cast<ALobbyPlayerController>(NewPlayer));
}

void ALobbyGameMode::Logout(AController* Exiting)
{
	Super::Logout(Exiting);
	
	LobbyGameState->RemovePlayer(Cast<ALobbyPlayerController>(Exiting));
}
