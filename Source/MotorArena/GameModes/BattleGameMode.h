/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#pragma once

#include "GameFramework/GameModeBase.h"
#include "BattleGameMode.generated.h"

/**
 * This Game mode is responsible for the main game initializing the rules, controllers and clients
 */
UCLASS()
class MOTORARENA_API ABattleGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	ABattleGameMode();

	// Colors methods
	void SetMotorcycleColors();
	class UMaterialInstance* GetRandomColor();

	// Colors properties
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BattleGameMode")
		TArray<class UMaterialInstance*> MotorcycleColors;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BattleGameMode")
		int CurrentColorIndex;

	TWeakObjectPtr<class ABattleGameState> BattleGameState;

protected:

	// Clients login and logout
	void PostLogin(APlayerController* NewPlayer) override;
	void Logout(AController* Exiting) override;

	void BeginPlay() override;

	void CreateAIs();
};
