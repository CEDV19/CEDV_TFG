/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#pragma once

#include "Components/ActorComponent.h"
#include "Net/UnrealNetwork.h"
#include "Engine/StaticMeshActor.h"
#include "WallGeneratorActorComponent.generated.h"

/**
 * The Actor Component which responsibility is create the WallActor class in the game
 */
UCLASS(Blueprintable, BlueprintType, ClassGroup=WallGeneratorComponent, meta=(BlueprintSpawnableComponent))
class MOTORARENA_API UWallGeneratorActorComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UWallGeneratorActorComponent(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable, Category = "WallGenerator")
		void CreateNewWallPart(FVector Location, FRotator Rotation, class UMaterialInstance* WallColor = nullptr);

	UFUNCTION(Server, Reliable, WithValidation)
		void SetWallPosition(FVector Location, FRotator Rotation);
	
	UFUNCTION(BlueprintCallable, Category = "WallGenerator")
		void RemoveAllWallGenerated();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WallGenerator)
		float WallBackPadding;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WallGenerator)
		float WallTopPadding;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WallGenerator)
		float WallEndPadding;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WallGenerator)
		float WallWidth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WallGenerator)
		float WallHeight;

	TWeakObjectPtr<class AWallActor> WallActor;
	TArray<TWeakObjectPtr<class AWallActor>> WallParts;
	

protected:

	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	bool CreatingNewWall;
};
