/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#include "WallGeneratorActorComponent.h"
#include "GameFramework/Actor.h"
#include "Actors/WallActor.h"
#include "Engine/World.h"

UWallGeneratorActorComponent::UWallGeneratorActorComponent(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Initialize class
	PrimaryComponentTick.bCanEverTick = true;
	WallBackPadding = 50;
	WallTopPadding = 100;
	WallEndPadding = 0.f;
	WallWidth = 0.05;
	WallHeight = 0.37f;
	CreatingNewWall = false;
	bReplicates = true;
}

void UWallGeneratorActorComponent::CreateNewWallPart(const FVector Location, const FRotator Rotation, class UMaterialInstance* WallColor)
{
	CreatingNewWall = true;
	FVector LastLocation = Location;
	if (WallActor.IsValid())
	{
		LastLocation = WallActor->Finalize(Location, WallEndPadding);
	}
	WallActor.Reset();
	WallActor = GetWorld()->SpawnActor<AWallActor>();
	WallActor->Initialize(
		GetOwner(),
		LastLocation,
		Rotation,
		WallColor,
		WallTopPadding,
		WallWidth,
		WallHeight
	);
	WallParts.Add(WallActor);
	CreatingNewWall = false;
}

void UWallGeneratorActorComponent::RemoveAllWallGenerated()
{
	for(auto Wall : WallParts)
		Wall->Destroy();
}

void UWallGeneratorActorComponent::SetWallPosition_Implementation(FVector Location, FRotator Rotation)
{
	if(WallActor.IsValid()) WallActor->SetLocationAndRotation(Location, Rotation);
}

bool UWallGeneratorActorComponent::SetWallPosition_Validate(FVector Location, FRotator Rotation)
{
	return true;
}

void UWallGeneratorActorComponent::TickComponent(float DeltaTime, ELevelTick TickType,
	FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(!CreatingNewWall && WallActor.IsValid())
	{
		if(GetOwner()->Role == ROLE_Authority)
			SetWallPosition(GetOwner()->GetActorLocation(), GetOwner()->GetActorRotation());
	}
}
