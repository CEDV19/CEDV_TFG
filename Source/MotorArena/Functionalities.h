/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GameFramework/Actor.h"
#include "Functionalities.generated.h"

/**
 * Auxiliary static class which contains methods needed in any part of the code 
 */
UCLASS()
class MOTORARENA_API UFunctionalities : public UObject
{
	GENERATED_BODY()
	
public:

	template<typename T>
	static FORCEINLINE void ShuffleArray(TArray<T>& Array)
	{
		for(uint8 Index = Array.Num() - 1; Index > 0; --Index)
		{
			uint8 RandomIndex = FMath::FloorToInt(FMath::SRand() * (Index + 1)) % Array.Num();
			auto TemporalObject = Array[Index];
			Array[Index] = Array[RandomIndex];
			Array[RandomIndex] = TemporalObject;
		}
	}

	template<typename T>
	static FORCEINLINE T GetRandomElementInArray(const TArray<T>& Array)
	{
		uint8 RandomIndex = FMath::Rand() % Array.Num();
		return Array[RandomIndex];
	}

	static AActor* MinActorDistance(
		const AActor* ActorSource,
		const TArray<AActor*>& ActorsToCheckDistance,
		float& MinDistance
	);
};
