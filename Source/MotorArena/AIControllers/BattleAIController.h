/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#pragma once

#include "AIController.h"
#include "BattleAIController.generated.h"

/**
 * The AI controller of the AI characters in the main game which responsibility is create the AI
 */
UCLASS()
class MOTORARENA_API ABattleAIController : public AAIController
{
	GENERATED_BODY()

public:
	ABattleAIController(const FObjectInitializer& ObjectInitializer);

	void OnPossess(APawn* InPawn) override;

	// AI methods
	UFUNCTION(BlueprintCallable, Category = "BattleAIController")
		void SetAIDifficultyStructure(const class ABattleGameState* BattleGameState, FString AI = "Easy");

	UFUNCTION(BlueprintCallable, Category = "BattleAIController")
		void StopAI();
	
protected:

	// AI variables
	TMap<FString, TSubclassOf<class AAIActor>> AIModes;
	TWeakObjectPtr<class AAIActor> AISelected;
};
